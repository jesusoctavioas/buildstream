.. BuildStream documentation master file, created by
   sphinx-quickstart on Mon Nov  7 21:03:37 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


BuildStream documentation
=========================
These docs cover everything you need to build and integrate software stacks
using BuildStream.

They begin with a basic introduction to BuildStream, background
information on basic concepts, and a guide to the BuildStream command line interface.
Later sections provide detailed information on BuilStream internals.


.. toctree::
   :maxdepth: 2

   about
   main_install
   main_using
   main_core
   resources
   HACKING
