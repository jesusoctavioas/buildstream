
.. _examples:

Examples
========
This page contains documentation for real examples of BuildStream projects,
described step by step. All run under CI, so you can trust they are
maintained and work as expected.


.. toctree::
   :maxdepth: 2

   examples_flatpak_autotools
