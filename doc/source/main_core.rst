

.. _main_core:

Reference documentation
=======================
This section details the core API reference along with
other more elaborate details about BuildStream internals.


.. toctree::
   :maxdepth: 2

   authoring
   cachekeys
   sandboxing
   core_framework
   modules
