

.. _main_using:

Using BuildStream
=================
This section details how to use the BuildStream command line interface and work with existing BuildStream projects.


.. toctree::
   :maxdepth: 2

   commands
   user_config
   examples
