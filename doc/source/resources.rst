.. _resources:


Resources
=========

* GitLab repository: https://gitlab.com/BuildStream/buildstream
* Bug Tracking: https://gitlab.com/BuildStream/buildstream/issues
* Mailing list: https://mail.gnome.org/mailman/listinfo/buildstream-list
* IRC Channel: irc://irc.gnome.org/#buildstream
